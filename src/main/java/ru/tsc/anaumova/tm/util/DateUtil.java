package ru.tsc.anaumova.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    String PATTERN = "dd.MM.yyyy";

    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    static Date toDate(final String value) {
        try {
            return FORMATTER.parse(value);
        } catch (final ParseException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    static String toString(final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }

}