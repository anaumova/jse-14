package ru.tsc.anaumova.tm.api.service;

import ru.tsc.anaumova.tm.enumerated.Sort;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService {

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAll(Sort sort);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    void clear();

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

}