package ru.tsc.anaumova.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProjectList();

    void createProject();

    void updateProjectByIndex();

    void updateProjectById();

    void removeProjectByIndex();

    void removeProjectById();

    void showProjectByIndex();

    void showProjectById();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();

    void startProjectByIndex();

    void startProjectById();

    void completeProjectByIndex();

    void completeProjectById();

}