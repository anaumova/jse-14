package ru.tsc.anaumova.tm.api.controller;

public interface ITaskController {

    void showTaskList();

    void showTaskListByProjectId();

    void clearTaskList();

    void createTask();

    void updateTaskByIndex();

    void updateTaskById();

    void removeTaskByIndex();

    void removeTaskById();

    void showTaskByIndex();

    void showTaskById();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();

    void startTaskByIndex();

    void startTaskById();

    void completeTaskByIndex();

    void completeTaskById();

}